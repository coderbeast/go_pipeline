package pipeline

import (
	"net"
	"log"
	"bufio"
)

func NetworkSink(addr string,in chan int )  {
	listen,err :=net.Listen("tcp",addr)
	if err!= nil {
		log.Printf("listen err")
		panic(err)
	}
	defer listen.Close()
	conn,err := listen.Accept()
	if err != nil {
		log.Printf("accept err")
		panic(err)
	}
	defer conn.Close()
	write :=bufio.NewWriter(conn)
	WriteSink(write,in)
}
func NewworkSource(addr string) <- chan int  {
	out :=make(chan int ,1024)
	go func() {
		conn,err :=net.Dial("tcp",addr)
		if err != nil {
			log.Println("net dial errr")
			panic(err)
		}
		readChan:=ReadSource(bufio.NewReader(conn),-1)
		for v := range readChan{
			out <- v
		}
		close(out)
	}()
}
