package main

import (
	"externalsort/pipeline"
	"fmt"
	"os"
	"bufio"
	"log"
	"unsafe"
	"math/rand"
)

func main ()  {
	const Filename="small.in"
	const Number=100000000
	a :=rand.Int()
	log.Printf("file with %d integer size is:%d\r\n",Number,Number*unsafe.Sizeof(a))
	file,err :=os.Create(Filename)
	defer file.Close()
	if err != nil {
		panic("file create err")
	}
	p := pipeline.RandomSource(Number)
	write :=bufio.NewWriter(file)
	pipeline.WriteSink(write,p)
	write.Flush()

	file,err = os.Open(Filename)
	defer file.Close()
	if err != nil {
		panic("file open err ")
	}
	p = pipeline.ReadSource(bufio.NewReader(file),-1)
	//for n := range p {
	//
	//	fmt.Println(n)
	//}

}
func Merge()  {
	p := pipeline.Merge(
		pipeline.InMemorySort(pipeline.ArraySource(3,2,6,4)),
		pipeline.InMemorySort(pipeline.ArraySource(8,0,5,2)),
	)
	//for {
	//	if num,ok := <-p;ok {
	//		fmt.Println(num)
	//	}else{
	//		break
	//	}
	//}
	for v := range p {
		fmt.Println(v)
	}
}